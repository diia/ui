import { Component } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { OnInit } from '@angular/core';
import {WebsocketService} from './core/websocket.service'
import { Http } from '@angular/http';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'OwnMusic';
  tracks = Array()
  track_results = Array()
  show_tracks: Boolean = true
  playing_track: Number
  userDataForm: FormGroup
  server_address = environment.server_address
  server_port = environment.server_port

  constructor(private ws:WebsocketService, private http: Http){}

  ngOnInit() {
    this.userDataForm = new FormGroup({
      name: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-ZñÑ ]*') ] ),
      telephone: new FormControl('', [ Validators.pattern('[0-9]{10}') ] ),
      maternal_surname: new FormControl('', [ Validators.pattern('[a-zA-ZñÑ]*') ] ),
      paternal_surname: new FormControl('', [ Validators.pattern('[a-zA-ZñÑ]*') ] )
    });

    this.ws
    .connect(`ws://${this.server_address}:${this.server_port}/ws`)
    .map((response: MessageEvent) => {
      return JSON.parse(response.data)
    }).subscribe(json => {			
      if(json.hasOwnProperty("all_tracks")){
        this.tracks = json.all_tracks
      }else if(json.hasOwnProperty('update_track')){
      
      }else if(json.hasOwnProperty('new_track')){
        let new_track = json.new_track
        this.tracks.push(new_track)
      }else if(json.hasOwnProperty('new_track_like')){
        let new_track_like = json.new_track_like
        let track = this.tracks.find(track => track.id === new_track_like.track.id)
        track.track_likes.forEach( (track_like, index) => {
          if(track_like.id === new_track_like.id) track.track_likes.splice(index,1);
        });
        track.track_likes.push(new_track_like)
      }else if(json.hasOwnProperty('personal_data')){
        let personal_data = json.personal_data
        this.userDataForm.get('name').setValue(personal_data.name)
        this.userDataForm.get('telephone').setValue(personal_data.telephone)
        this.userDataForm.get('maternal_surname').setValue(personal_data.maternal_surname)
        this.userDataForm.get('paternal_surname').setValue(personal_data.paternal_surname)        
      }else if(json.hasOwnProperty('track_playing')){
        this.playing_track = json.track_playing     
      }
    });
    
  }

  playlist_add(track_type, track_id){
    let type_str = (track_type==1) ? "local" : "spotify"
    let url = `http://${this.server_address}:${this.server_port}/play_list/add_track/${track_id}/${type_str}`
    this.http.get(url).subscribe(res => {
      if(res.status === 200){
        this.show_tracks = true 
      }
    })
  }

  search_track(search_text){
    let url = `http://${this.server_address}:${this.server_port}/play_list/search_track/all/${search_text}`
    this.http.get(url).subscribe(res => {
      this.track_results = res.json()
      this.show_tracks = false
    })

  }

  track_likes(track_id, like_type) {
    let track = this.tracks.find(track => track.id === track_id)
    return track.track_likes.filter(track_like => track_like.state === like_type).length
  }

  like_track(track_id, like_type) {
    let like_type_str = (like_type==1) ? "like" : "dislike"
    let url = `http://${this.server_address}:${this.server_port}/play_list/new_likestate/${track_id}/${like_type_str}`
    this.http.get(url).subscribe(res => {

    },
    err => {
      console.log(err.status)
    }) 
  }

  spotify_artists(spotify_track){
    return spotify_track.artists.map(track => " "+track.name);
  }

  saveUserData(){
    let validFields = {}
    Object.keys( this.userDataForm.controls).forEach(key => {
      let field = this.userDataForm.get(key)
      if(field.valid){
        validFields[key] = field.value
      }
    })

    let url = `http://${this.server_address}:${this.server_port}/user/personal_data`
    this.http.put(url, validFields).subscribe(res => {
      console.log(res.text())
    },
    err => {
      console.log(err.status)
    }) 
    
  }
}
